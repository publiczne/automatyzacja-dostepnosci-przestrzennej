"""
Model exported as python.
Name : 3. Selekcja dróg z siatki drogowej
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterMapLayer
from qgis.core import QgsProcessingParameterExpression
from qgis.core import QgsProcessingParameterFeatureSink
import processing


class SelekcjaDrgZSiatkiDrogowej(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterMapLayer('Warstwazsiatkdrg', 'Siatka drogowa – reprezentacja liniowa', defaultValue=None, types=[QgsProcessing.TypeVectorLine]))
        self.addParameter(QgsProcessingParameterExpression('Wyraeniefiltra', 'Wyrażenie filtra', parentLayerParameterName='', defaultValue='\"highway\" not in (\'motorway\', \'motorway_link\', \'trunk\', \'trunk_link\', \'proposed\', \'construction\')'))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania', 'Siatka drogowa – obiekty niespełniające warunku filtrowania', optional=True, type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=False, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaObiektySpeniajceWarunekFiltrowania', 'Siatka drogowa – obiekty spełniające warunek filtrowania', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(1, model_feedback)
        results = {}
        outputs = {}

        # Wyodrębnij za pomocą wyrażenia
        alg_params = {
            'EXPRESSION': parameters['Wyraeniefiltra'],
            'INPUT': parameters['Warstwazsiatkdrg'],
            'FAIL_OUTPUT': parameters['SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania'],
            'OUTPUT': parameters['SiatkaDrogowaObiektySpeniajceWarunekFiltrowania']
        }
        outputs['WyodrbnijZaPomocWyraenia'] = processing.run('native:extractbyexpression', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania'] = outputs['WyodrbnijZaPomocWyraenia']['FAIL_OUTPUT']
        results['SiatkaDrogowaObiektySpeniajceWarunekFiltrowania'] = outputs['WyodrbnijZaPomocWyraenia']['OUTPUT']
        return results

    def name(self):
        return '3. Selekcja dróg z siatki drogowej'

    def displayName(self):
        return '3. Selekcja dróg z siatki drogowej'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p>Algorytm służy do selekcji z siatki drogowej jedynie części dróg.

Użytkownik sam definuje ustawienie filtra korsyztając z zapytania SQL.

Zarówno wyniki pasujące do zapytania, jak i nie pasujące zostają zwrócone jako warstwy.</p>
<h2>Parametry wejściowe</h2>
<h3>Verbose logging</h3>
<p></p>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p>Wskaż warstwę z siatką drogową dla której ma zostać wykonane filtrowania.</p>
<h3>Wyrażenie filtra</h3>
<p>Podaj zapytanie, które posłuży jako filtr.

Przykładowe zapytania:
—  odrzuci z daleszej analizy autostrady, drogi ekpresowe, ich łącznice, drogi planowne i w budowie (domyślne, przydatne dla analizy dostępności pieszej):
"highway" not in ('motorway', 'motorway_link', 'trunk', 'trunk_link', 'proposed', 'construction')
— odrzuci z dalszej analizy drogi planowane i w budowie oraz ciągi nieprzeznaczone dla ruchu samochodów (ścieżki, chodniki, drogi rowerowe) ( przydatne do analizy dostępności samochodowej):
"highway" not in ('proposed', 'construction', 'path', 'footway', 'cycleway')

Usuwając drogi w budowie ("highway"='construction') należy zachować szczególną ostrożność, gdzyż tym samym tagiem ozanczane są drogi w przebudowie lub remoncie, które jednak dalej częściowo pozostają przejezdne, a w związku z tym zapewniają obsługę dla terenów położonych bezpośrednio przy nich lub położonych dalej. Najczęściej konieczna jest manualna weryfikacja danych.</p>
<h3>Siatka drogowa – obiekty niespełniające warunku filtrowania</h3>
<p>Warstwa z obiektami niespełniającymi warunków zapyania.</p>
<h3>Siatka drogowa – obiekty spełniające warunek filtrowania</h3>
<p>Warstwa z obiektami spełniającymi warunków zapyania.</p>
<h2>Wyniki</h2>
<h3>Siatka drogowa – obiekty niespełniające warunku filtrowania</h3>
<p>Warstwa z obiektami niespełniającymi warunków zapyania.</p>
<h3>Siatka drogowa – obiekty spełniające warunek filtrowania</h3>
<p>Warstwa z obiektami spełniającymi warunków zapyania.</p>
<br><p align="right">Autor algorytmu: Andrzej Bąk</p><p align="right">Autor pomocy: Andrzej Bąk</p><p align="right">Wersja algorytmu: 0.1</p></body></html>"""

    def createInstance(self):
        return SelekcjaDrgZSiatkiDrogowej()
