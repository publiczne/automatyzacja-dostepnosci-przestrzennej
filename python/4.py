"""
Model exported as python.
Name : 4. Wzynaczanie izopoligonów dostępności (z warstwy)
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterFeatureSource
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsProcessingParameterRasterDestination
import processing


class WzynaczanieIzopoligonwDostpnociZWarstwy(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterNumber('Interwaizolini', 'Interwał izopoligonów', type=QgsProcessingParameterNumber.Double, minValue=0, maxValue=1.79769e+308, defaultValue=100))
        self.addParameter(QgsProcessingParameterNumber('Maksymalnaodlegoanalizyodpunktwpocztkowych', 'Maksymalna odległość analizy od punktów początkowych', type=QgsProcessingParameterNumber.Double, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSource('Punktypocztkowe', 'Analizowane obiekty – reprezentacja punktowa', types=[QgsProcessing.TypeVectorPoint], defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterFeatureSource('Warstwasiatkidorogwej', 'Siatka drogowa – reprezentacja liniowa', types=[QgsProcessing.TypeVectorAnyGeometry], defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('PoligonyZasiguDostpnoci', 'Poligony zasięgu dostępności', type=QgsProcessing.TypeVectorPolygon, createByDefault=True, defaultValue='TEMPORARY_OUTPUT'))
        self.addParameter(QgsProcessingParameterRasterDestination('ZinterpolowanaDostpno', 'Zinterpolowana dostępność', createByDefault=True, defaultValue=''))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(1, model_feedback)
        results = {}
        outputs = {}

        # Iso-Area as Polygons (from Layer)
        alg_params = {
            'CELL_SIZE': 10,
            'DEFAULT_DIRECTION': 2,
            'DEFAULT_SPEED': 10,
            'DIRECTION_FIELD': '',
            'ENTRY_COST_CALCULATION_METHOD': 0,
            'ID_FIELD': 'AUTOID',
            'INPUT': parameters['Warstwasiatkidorogwej'],
            'INTERVAL': parameters['Interwaizolini'],
            'MAX_DIST': parameters['Maksymalnaodlegoanalizyodpunktwpocztkowych'],
            'SPEED_FIELD': '',
            'START_POINTS': parameters['Punktypocztkowe'],
            'STRATEGY': 0,
            'TOLERANCE': 10,
            'VALUE_BACKWARD': '',
            'VALUE_BOTH': '',
            'VALUE_FORWARD': '',
            'OUTPUT_INTERPOLATION': parameters['ZinterpolowanaDostpno'],
            'OUTPUT_POLYGONS': parameters['PoligonyZasiguDostpnoci']
        }
        outputs['IsoareaAsPolygonsFromLayer'] = processing.run('qneat3:isoareaaspolygonsfromlayer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['PoligonyZasiguDostpnoci'] = outputs['IsoareaAsPolygonsFromLayer']['OUTPUT_POLYGONS']
        results['ZinterpolowanaDostpno'] = outputs['IsoareaAsPolygonsFromLayer']['OUTPUT_INTERPOLATION']
        return results

    def name(self):
        return '4. Wzynaczanie izopoligonów dostępności (z warstwy)'

    def displayName(self):
        return '4. Wzynaczanie izopoligonów dostępności (z warstwy)'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p></p>
<h2>Parametry wejściowe</h2>
<h3>Interwał izopoligonów</h3>
<p>	Odległość co jaką algorytm wyznaczania dostępności wykreśli granicę izopoligonów dostępności.</p>
<h3>Maksymalna odległość analizy od punktów początkowych</h3>
<p></p>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<h3>Verbose logging</h3>
<p></p>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p></p>
<h3>Poligony zasięgu dostępności</h3>
<p></p>
<h3>Zinterpolowana dostępność</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Poligony zasięgu dostępności</h3>
<p></p>
<h3>Zinterpolowana dostępność</h3>
<p></p>
<br></body></html>"""

    def createInstance(self):
        return WzynaczanieIzopoligonwDostpnociZWarstwy()
