"""
Model exported as python.
Name : 1. Pobieranie obiektów do analizy
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterCrs
from qgis.core import QgsProcessingParameterString
from qgis.core import QgsProcessingParameterFeatureSource
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSink
import processing


class PobieranieObiektwDoAnalizy(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterCrs('Ukadwsprzdnych', 'Układ współrzędnych', defaultValue='EPSG:2180'))
        self.addParameter(QgsProcessingParameterString('Obiektydoanalizy (2)', 'Klucz OSM', multiLine=False, defaultValue='amenity'))
        self.addParameter(QgsProcessingParameterString('Obiektydoanalizy', 'Obiekty do analizy', optional=True, multiLine=False, defaultValue='school'))
        self.addParameter(QgsProcessingParameterFeatureSource('Test', 'Obszar do analizy', types=[QgsProcessing.TypeVectorAnyGeometry], defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterFeatureSink('AnalizowaneObiektyReprezentacjaPunktowa', 'Analizowane obiekty – reprezentacja punktowa', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(10, model_feedback)
        results = {}
        outputs = {}

        # Zasięg z obszaru analizy
        alg_params = {
            'INPUT': parameters['Test'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ZasigZObszaruAnalizy'] = processing.run('native:extenttolayer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Zapytanie do OSM
        alg_params = {
            'EXTENT': outputs['ZasigZObszaruAnalizy']['OUTPUT'],
            'KEY': parameters['Obiektydoanalizy (2)'],
            'SERVER': 'https://lz4.overpass-api.de/api/interpreter',
            'TIMEOUT': 25,
            'VALUE': parameters['Obiektydoanalizy']
        }
        outputs['ZapytanieDoOsm'] = processing.run('quickosm:buildqueryextent', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Pobieranie obiektów do analizy
        alg_params = {
            'URL': outputs['ZapytanieDoOsm']['OUTPUT_URL'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PobieranieObiektwDoAnalizy'] = processing.run('native:filedownloader', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Otwórz wszystkie podwarstwy obiektów z pliku OSM
        alg_params = {
            'FILE': outputs['PobieranieObiektwDoAnalizy']['OUTPUT'],
            'OSM_CONF': ''
        }
        outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm'] = processing.run('quickosm:openosmfile', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        # Centroidy z multilinii
        alg_params = {
            'ALL_PARTS': True,
            'INPUT': outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_MULTILINESTRINGS'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['CentroidyZMultilinii'] = processing.run('native:centroids', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(5)
        if feedback.isCanceled():
            return {}

        # Centroidy z multipoligonów
        alg_params = {
            'ALL_PARTS': True,
            'INPUT': outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_MULTIPOLYGONS'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['CentroidyZMultipoligonw'] = processing.run('native:centroids', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(6)
        if feedback.isCanceled():
            return {}

        # Centroidy z linii
        alg_params = {
            'ALL_PARTS': True,
            'INPUT': outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_LINES'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['CentroidyZLinii'] = processing.run('native:centroids', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(7)
        if feedback.isCanceled():
            return {}

        # Złączone punkty reprezentujące obiekty
        alg_params = {
            'CRS': parameters['Ukadwsprzdnych'],
            'LAYERS': [outputs['CentroidyZLinii']['OUTPUT'],outputs['CentroidyZMultilinii']['OUTPUT'],outputs['CentroidyZMultipoligonw']['OUTPUT'],outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_POINTS']],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ZczonePunktyReprezentujceObiekty'] = processing.run('native:mergevectorlayers', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(8)
        if feedback.isCanceled():
            return {}

        # Wyodrębnij przestrzennie
        alg_params = {
            'INPUT': outputs['ZczonePunktyReprezentujceObiekty']['OUTPUT'],
            'INTERSECT': parameters['Test'],
            'PREDICATE': [1,6],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['WyodrbnijPrzestrzennie'] = processing.run('native:extractbylocation', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(9)
        if feedback.isCanceled():
            return {}

        # Dodaj pole z autonumeracją
        alg_params = {
            'FIELD_NAME': 'AUTOID',
            'GROUP_FIELDS': [''],
            'INPUT': outputs['WyodrbnijPrzestrzennie']['OUTPUT'],
            'SORT_ASCENDING': True,
            'SORT_EXPRESSION': '',
            'SORT_NULLS_FIRST': False,
            'START': 0,
            'OUTPUT': parameters['AnalizowaneObiektyReprezentacjaPunktowa']
        }
        outputs['DodajPoleZAutonumeracj'] = processing.run('native:addautoincrementalfield', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['AnalizowaneObiektyReprezentacjaPunktowa'] = outputs['DodajPoleZAutonumeracj']['OUTPUT']
        return results

    def name(self):
        return '1. Pobieranie obiektów do analizy'

    def displayName(self):
        return '1. Pobieranie obiektów do analizy'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p>Algorytm pobiera z określonego przez użytkownika obszaru obiekty z OpenStreetMap które spełniają określoną przez użytkownika wartość klucza "amenity".

Dla obiektów o typach geometrii linia, multilinia, multipoligon wyznaczane są centroidy. Przy czym każda część obiektu złożonego traktowana jest oddzielnie.

Następnie tak uzyskanene centroidy są złączane wraz z obiektami reprezentowanymi przez dane punktowe. Nadawany jest im unikatowy numer i tworzona jest warstwa wyjściowa.</p>
<h2>Parametry wejściowe</h2>
<h3>Układ współrzędnych</h3>
<p>Układ współrzędnych (jednostami nie mogą być kąty za to mogą być np. metry).</p>
<h3>Klucz OSM</h3>
<p>Typ klucz dla tagów OpenStreetMap.

Tagi OSM przyjmują postać "klucz"='wartość'. Tu należy zdefiniować pierwszą z nich.

Domyślnie "amenity" co jest odpowiednie dla wyszukiwania obiektów użyteczności.</p>
<h3>Obiekty do analizy</h3>
<p>Wskaż typ analizowanych obiektów (wartości klucza "amenity" z OpenStreetMap).

Lista wartości klucza "amenity" znajduje się na stronie: https://wiki.openstreetmap.org/wiki/Pl:Key:amenity</p>
<h3>Obszar do analizy</h3>
<p></p>
<h3>Verbose logging</h3>
<p></p>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<br><p align="right">Autor algorytmu: Andrzej Bąk</p><p align="right">Autor pomocy: Andrzej Bąk</p><p align="right">Wersja algorytmu: 0.1</p></body></html>"""

    def createInstance(self):
        return PobieranieObiektwDoAnalizy()
