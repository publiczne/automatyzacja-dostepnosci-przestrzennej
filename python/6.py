"""
Model exported as python.
Name : 6. Próbkowanie rastrowej warstwy dostępności za pomocą punktów reprezentujących zabudowania i ich wstępny podział na mieszczące się w akceptowalny zasięg dostępności lub nie
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSource
from qgis.core import QgsProcessingParameterRasterLayer
from qgis.core import QgsProcessingParameterFeatureSink
import processing


class PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasigDostpnociLubNie(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterNumber('Akceptowalnyzasigdostpnoci', 'Akceptowalny zasięg dostępności', type=QgsProcessingParameterNumber.Double, defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterFeatureSource('Warstwapunktowazobiektamidoanalizy', 'Analizowane zabudowania – reprezentacja punktowa', types=[QgsProcessing.TypeVectorPoint], defaultValue=None))
        self.addParameter(QgsProcessingParameterRasterLayer('Zinterpolowanyrasterzodlegociami', 'Zinterpolowana dostępność', defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('ZabudowaniaWAkceptowalnymZasiguDostpnoci', 'Zabudowania w akceptowalnym zasięgu dostępności', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci', 'Zabudowania poza akceptowalnym zasięgiem dostępności', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('Zabudowania', 'Zabudowania', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(4, model_feedback)
        results = {}
        outputs = {}

        # Próbkuj wartości rastra
        alg_params = {
            'COLUMN_PREFIX': 'SODL',
            'INPUT': parameters['Warstwapunktowazobiektamidoanalizy'],
            'RASTERCOPY': parameters['Zinterpolowanyrasterzodlegociami'],
            'OUTPUT': parameters['Zabudowania']
        }
        outputs['PrbkujWartociRastra'] = processing.run('qgis:rastersampling', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['Zabudowania'] = outputs['PrbkujWartociRastra']['OUTPUT']

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Czy obiekty mieszczą się w akceptowalnej dostępności
        alg_params = {
            'FIELD_LENGTH': 1,
            'FIELD_NAME': 'W_Zas',
            'FIELD_PRECISION': 1,
            'FIELD_TYPE': 1,
            'FORMULA': 'if(\"SODL_1\"<=@Akceptowalnyzasigdostpnoci, 1, 0)\t',
            'INPUT': outputs['PrbkujWartociRastra']['OUTPUT'],
            'NEW_FIELD': True,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['CzyObiektyMieszczSiWAkceptowalnejDostpnoci'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Wyodrębnij zabudowania w akceptowalnym zasięgu dostępności
        alg_params = {
            'EXPRESSION': '\"W_Zas\"=\'1\'',
            'INPUT': outputs['CzyObiektyMieszczSiWAkceptowalnejDostpnoci']['OUTPUT'],
            'OUTPUT': parameters['ZabudowaniaWAkceptowalnymZasiguDostpnoci']
        }
        outputs['WyodrbnijZabudowaniaWAkceptowalnymZasiguDostpnoci'] = processing.run('native:extractbyexpression', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['ZabudowaniaWAkceptowalnymZasiguDostpnoci'] = outputs['WyodrbnijZabudowaniaWAkceptowalnymZasiguDostpnoci']['OUTPUT']

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Wyodrębnij zabudowania poza akceptowalnym zasięgiem dostępności
        alg_params = {
            'EXPRESSION': '\"W_Zas\"=\'0\'',
            'INPUT': outputs['CzyObiektyMieszczSiWAkceptowalnejDostpnoci']['OUTPUT'],
            'OUTPUT': parameters['ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci']
        }
        outputs['WyodrbnijZabudowaniaPozaAkceptowalnymZasigiemDostpnoci'] = processing.run('native:extractbyexpression', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci'] = outputs['WyodrbnijZabudowaniaPozaAkceptowalnymZasigiemDostpnoci']['OUTPUT']
        return results

    def name(self):
        return '6. Próbkowanie rastrowej warstwy dostępności za pomocą punktów reprezentujących zabudowania i ich wstępny podział na mieszczące się w akceptowalny zasięg dostępności lub nie'

    def displayName(self):
        return '6. Próbkowanie rastrowej warstwy dostępności za pomocą punktów reprezentujących zabudowania i ich wstępny podział na mieszczące się w akceptowalny zasięg dostępności lub nie'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p></p>
<h2>Parametry wejściowe</h2>
<h3>Akceptowalny zasięg dostępności</h3>
<p></p>
<h3>Verbose logging</h3>
<p></p>
<h3>Analizowane zabudowania – reprezentacja punktowa</h3>
<p></p>
<h3>Zinterpolowana dostępność</h3>
<p></p>
<h3>Zabudowania w akceptowalnym zasięgu dostępności</h3>
<p></p>
<h3>Zabudowania poza akceptowalnym zasięgiem dostępności</h3>
<p></p>
<h3>Zabudowania</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Zabudowania w akceptowalnym zasięgu dostępności</h3>
<p></p>
<h3>Zabudowania poza akceptowalnym zasięgiem dostępności</h3>
<p></p>
<h3>Zabudowania</h3>
<p></p>
<br></body></html>"""

    def createInstance(self):
        return PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasigDostpnociLubNie()
