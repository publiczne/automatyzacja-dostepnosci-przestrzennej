"""
Model exported as python.
Name : 2. Pobieranie siatki dróg
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterCrs
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterMapLayer
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsExpression
import processing


class PobieranieSiatkiDrg(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterCrs('Ukadwsprzdnych', 'Układ współrzędnych', defaultValue='EPSG:2180'))
        self.addParameter(QgsProcessingParameterNumber('Maksymalnyzasigwyznaczaniadostpnociodobiektu', 'Odległość analizy od znalezionych obiektów', type=QgsProcessingParameterNumber.Double, minValue=500, maxValue=10000, defaultValue=2500))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterMapLayer('Warstwazobiektamidoanalizy', 'Analizowane obiekty – reprezentacja punktowa', defaultValue=None, types=[QgsProcessing.TypeVectorPoint]))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaReprezentacjaLiniowa', 'Siatka drogowa – reprezentacja liniowa', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue='TEMPORARY_OUTPUT'))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(7, model_feedback)
        results = {}
        outputs = {}

        # Bufor
        alg_params = {
            'DISSOLVE': True,
            'DISTANCE': QgsExpression(' @Maksymalnyzasigwyznaczaniadostpnociodobiektu *1.25').evaluate(),
            'END_CAP_STYLE': 0,
            'INPUT': parameters['Warstwazobiektamidoanalizy'],
            'JOIN_STYLE': 0,
            'MITER_LIMIT': 2,
            'SEGMENTS': 5,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['Bufor'] = processing.run('native:buffer', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Zapytanie do OSM
        alg_params = {
            'EXTENT': outputs['Bufor']['OUTPUT'],
            'KEY': 'highway',
            'SERVER': 'https://lz4.overpass-api.de/api/interpreter',
            'TIMEOUT': 25,
            'VALUE': ''
        }
        outputs['ZapytanieDoOsm'] = processing.run('quickosm:buildqueryextent', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Pobieranie obiektów do analizy
        alg_params = {
            'URL': outputs['ZapytanieDoOsm']['OUTPUT_URL'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PobieranieObiektwDoAnalizy'] = processing.run('native:filedownloader', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Otwórz wszystkie podwarstwy obiektów z pliku OSM
        alg_params = {
            'FILE': outputs['PobieranieObiektwDoAnalizy']['OUTPUT'],
            'OSM_CONF': ''
        }
        outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm'] = processing.run('quickosm:openosmfile', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        # Rozbij multilinie na linie
        alg_params = {
            'INPUT': outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_MULTILINESTRINGS'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['RozbijMultilinieNaLinie'] = processing.run('native:multiparttosingleparts', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(5)
        if feedback.isCanceled():
            return {}

        # Złącz linie reprezentujące sieć drogową
        alg_params = {
            'CRS': parameters['Ukadwsprzdnych'],
            'LAYERS': [outputs['RozbijMultilinieNaLinie']['OUTPUT'],outputs['OtwrzWszystkiePodwarstwyObiektwZPlikuOsm']['OUTPUT_LINES']],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ZczLinieReprezentujceSieDrogow'] = processing.run('native:mergevectorlayers', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(6)
        if feedback.isCanceled():
            return {}

        # Dodaj pole z autonumeracją
        alg_params = {
            'FIELD_NAME': 'AUTOID',
            'GROUP_FIELDS': [''],
            'INPUT': outputs['ZczLinieReprezentujceSieDrogow']['OUTPUT'],
            'SORT_ASCENDING': True,
            'SORT_EXPRESSION': '',
            'SORT_NULLS_FIRST': False,
            'START': 1,
            'OUTPUT': parameters['SiatkaDrogowaReprezentacjaLiniowa']
        }
        outputs['DodajPoleZAutonumeracj'] = processing.run('native:addautoincrementalfield', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['SiatkaDrogowaReprezentacjaLiniowa'] = outputs['DodajPoleZAutonumeracj']['OUTPUT']
        return results

    def name(self):
        return '2. Pobieranie siatki dróg'

    def displayName(self):
        return '2. Pobieranie siatki dróg'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p>Algorytm pobiera dla określonego przez użytkownika zasięgu od analizowanych obiektów dane o sieci drogowej z OpenStreetMap .

Obiekty typu multilinia zostają rozbite na linie.

Następnie tak uzyskane linie są złączane wraz z liniami pozyskanymi bezpośrednio z OpenStreetMap. Nadawany jest im unikatowy numer i tworzona jest warstwa wyjściowa.</p>
<h2>Parametry wejściowe</h2>
<h3>Układ współrzędnych</h3>
<p>Układ współrzędnych (jednostami nie mogą być kąty za to mogą być np. metry).</p>
<h3>Odległość analizy od znalezionych obiektów</h3>
<p>Podaj maksymalny zasięg od obiektu dla jakiego będziesz chciał przeprowadzać analizę dostępności.

Ze wględów wydajnościowych wprowadzono ograniczenie wynoszące od 500 do 10000 jednostek projektu.</p>
<h3>Verbose logging</h3>
<p></p>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p>Wskaż warstwę punktową z obiektami dla których ma zostać przygotowana siatka drogowa.</p>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p></p>
<br><p align="right">Autor algorytmu: Andrzej Bąk</p><p align="right">Autor pomocy: Andrzej Bąk</p><p align="right">Wersja algorytmu: 0.1</p></body></html>"""

    def createInstance(self):
        return PobieranieSiatkiDrg()
