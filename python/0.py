"""
Model exported as python.
Name : Wyznaczanie dostępności – pełny algorytm
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterCrs
from qgis.core import QgsProcessingParameterFeatureSource
from qgis.core import QgsProcessingParameterString
from qgis.core import QgsProcessingParameterDistance
from qgis.core import QgsProcessingParameterExpression
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsProcessingParameterRasterDestination
import processing


class WyznaczanieDostpnociPenyAlgorytm(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterCrs('Ukadwsprzdnych', 'Układ współrzędnych', defaultValue='EPSG:2180'))
        self.addParameter(QgsProcessingParameterFeatureSource('Obszaranalizy', 'Obszary do analizy', types=[QgsProcessing.TypeVectorPolygon], defaultValue=None))
        self.addParameter(QgsProcessingParameterString('Obiektydoanalizy (2)', 'Klucz OSM', multiLine=False, defaultValue='amenity'))
        self.addParameter(QgsProcessingParameterString('Obiektydoanalizy', 'Obiekty do analizy', multiLine=False, defaultValue='school'))
        self.addParameter(QgsProcessingParameterDistance('Maksymalnaodlegoanalizyodznalezionychobiektw', 'Odległość analizy od znalezionych obiektów', parentParameterName='Ukadwsprzdnych', minValue=50, maxValue=10000, defaultValue=2500))
        self.addParameter(QgsProcessingParameterExpression('Wyraeniefilta', 'Wyrażenie filta', parentLayerParameterName='', defaultValue='\"highway\" not in (\'motorway\', \'motorway_link\', \'trunk\', \'trunk_link\', \'proposed\', \'construction\')'))
        self.addParameter(QgsProcessingParameterNumber('Interwaizolini', 'Interwał izopoligonów', type=QgsProcessingParameterNumber.Double, minValue=0, maxValue=1.79769e+308, defaultValue=100))
        self.addParameter(QgsProcessingParameterDistance('Maksymalnaodlegoanalizyodznalezionychobiektw (2)', 'Akceptowalny zasięg dostępności', parentParameterName='Ukadwsprzdnych', minValue=50, maxValue=10000, defaultValue=500))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterFeatureSink('AnalizowaneObiektyReprezentacjaPunktowa', 'Analizowane obiekty – reprezentacja punktowa', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaReprezentacjaLiniowa', 'Siatka drogowa – reprezentacja liniowa', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, supportsAppend=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania', 'Siatka drogowa – obiekty niespełniające warunku filtrowania', optional=True, type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=False, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('SiatkaDrogowaObiektySpeniajceWarunekFitrowania', 'Siatka drogowa – obiekty spełniające warunek fitrowania', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('PoligonyZasiguDostpnoci', 'Poligony zasięgu dostępności', type=QgsProcessing.TypeVectorPolygon, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterRasterDestination('ZinterpolowanaDostpno', 'Zinterpolowana dostępność', createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('Zabudowania', 'Zabudowania', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci', 'Zabudowania poza akceptowalnym zasięgiem dostępności', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('ZabudowaniaWAkceptowalnymZasiguDostpnoci', 'Zabudowania w akceptowalnym zasięgu dostępności', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('PodsumowanieAnalizy', 'Podsumowanie analizy', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(7, model_feedback)
        results = {}
        outputs = {}

        # 1. Pobieranie obiektów do analizy
        alg_params = {
            'Obiektydoanalizy': parameters['Obiektydoanalizy'],
            'Obiektydoanalizy (2)': parameters['Obiektydoanalizy (2)'],
            'Test': parameters['Obszaranalizy'],
            'Ukadwsprzdnych': parameters['Ukadwsprzdnych'],
            'native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa': parameters['AnalizowaneObiektyReprezentacjaPunktowa']
        }
        outputs['PobieranieObiektwDoAnalizy'] = processing.run('model:1. Pobieranie obiektów do analizy', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['AnalizowaneObiektyReprezentacjaPunktowa'] = outputs['PobieranieObiektwDoAnalizy']['native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa']

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # 2. Pobieranie siatki dróg
        alg_params = {
            'Maksymalnyzasigwyznaczaniadostpnociodobiektu': parameters['Maksymalnaodlegoanalizyodznalezionychobiektw'],
            'Ukadwsprzdnych': parameters['Ukadwsprzdnych'],
            'Warstwazobiektamidoanalizy': outputs['PobieranieObiektwDoAnalizy']['native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa'],
            'native:addautoincrementalfield_1:Siatka drogowa – reprezentacja liniowa': parameters['SiatkaDrogowaReprezentacjaLiniowa']
        }
        outputs['PobieranieSiatkiDrg'] = processing.run('model:2. Pobieranie siatki dróg', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['SiatkaDrogowaReprezentacjaLiniowa'] = outputs['PobieranieSiatkiDrg']['native:addautoincrementalfield_1:Siatka drogowa – reprezentacja liniowa']

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # 3. Selekcja dróg z siatki drogowej
        alg_params = {
            'Warstwazsiatkdrg': outputs['PobieranieSiatkiDrg']['native:addautoincrementalfield_1:Siatka drogowa – reprezentacja liniowa'],
            'Wyraeniefiltra': parameters['Wyraeniefilta'],
            'native:extractbyexpression_1:Siatka drogowa – obiekty niespełniające warunku filtrowania': parameters['SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania'],
            'native:extractbyexpression_1:Siatka drogowa – obiekty spełniające warunek filtrowania': parameters['SiatkaDrogowaObiektySpeniajceWarunekFitrowania']
        }
        outputs['SelekcjaDrgZSiatkiDrogowej'] = processing.run('model:3. Selekcja dróg z siatki drogowej', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['SiatkaDrogowaObiektyNiespeniajceWarunkuFiltrowania'] = outputs['SelekcjaDrgZSiatkiDrogowej']['native:extractbyexpression_1:Siatka drogowa – obiekty niespełniające warunku filtrowania']
        results['SiatkaDrogowaObiektySpeniajceWarunekFitrowania'] = outputs['SelekcjaDrgZSiatkiDrogowej']['native:extractbyexpression_1:Siatka drogowa – obiekty spełniające warunek filtrowania']

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # 4. Wyznaczanie izopoligonów dostępności
        alg_params = {
            'Interwaizolini': parameters['Interwaizolini'],
            'Maksymalnaodlegoanalizyodpunktwpocztkowych': parameters['Maksymalnaodlegoanalizyodznalezionychobiektw'],
            'Punktypocztkowe': outputs['PobieranieObiektwDoAnalizy']['native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa'],
            'Warstwasiatkidorogwej': outputs['SelekcjaDrgZSiatkiDrogowej']['native:extractbyexpression_1:Siatka drogowa – obiekty spełniające warunek filtrowania'],
            'qneat3:isoareaaspolygonsfromlayer_1:Poligony zasięgu dostępności': parameters['PoligonyZasiguDostpnoci'],
            'qneat3:isoareaaspolygonsfromlayer_1:Zinterpolowana dostępność': parameters['ZinterpolowanaDostpno']
        }
        outputs['WyznaczanieIzopoligonwDostpnoci'] = processing.run('model:4. Wzynaczanie izopoligonów dostępności (z warstwy)', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['PoligonyZasiguDostpnoci'] = outputs['WyznaczanieIzopoligonwDostpnoci']['qneat3:isoareaaspolygonsfromlayer_1:Poligony zasięgu dostępności']
        results['ZinterpolowanaDostpno'] = outputs['WyznaczanieIzopoligonwDostpnoci']['qneat3:isoareaaspolygonsfromlayer_1:Zinterpolowana dostępność']

        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        # 5. Pobieranie zabudowań do analizy (ten sam algorytm co w pkt. 1, ale z innymi danymi wejściowymi).
        alg_params = {
            'Obiektydoanalizy': '',
            'Obiektydoanalizy (2)': 'building',
            'Test': parameters['Obszaranalizy'],
            'Ukadwsprzdnych': parameters['Ukadwsprzdnych'],
            'native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PobieranieZabudowaDoAnalizyTenSamAlgorytmCoWPkt1AleZInnymiDanymiWejciowymi'] = processing.run('model:1. Pobieranie obiektów do analizy', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(5)
        if feedback.isCanceled():
            return {}

        # 6. Próbkowanie rastrowej warstwy dostępności za pomocą punktów reprezentujących zabudowania i ich wstępny podział na mieszczące się w akceptowalny zasięgu dostępności lub nie
        alg_params = {
            'Akceptowalnyzasigdostpnoci': parameters['Maksymalnaodlegoanalizyodznalezionychobiektw (2)'],
            'Warstwapunktowazobiektamidoanalizy': outputs['PobieranieZabudowaDoAnalizyTenSamAlgorytmCoWPkt1AleZInnymiDanymiWejciowymi']['native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa'],
            'Zinterpolowanyrasterzodlegociami': outputs['WyznaczanieIzopoligonwDostpnoci']['qneat3:isoareaaspolygonsfromlayer_1:Zinterpolowana dostępność'],
            'native:extractbyexpression_1:Zabudowania w akceptowalnym zasięgu dostępności': parameters['ZabudowaniaWAkceptowalnymZasiguDostpnoci'],
            'native:extractbyexpression_2:Zabudowania poza akceptowalnym zasięgiem dostępności': parameters['ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci'],
            'qgis:rastersampling_1:Zabudowania': parameters['Zabudowania']
        }
        outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie'] = processing.run('model:6. Próbkowanie rastrowej warstwy dostępności za pomocą punktów reprezentujących zabudowania i ich wstępny podział na mieszczące się w akceptowalny zasięg dostępności lub nie', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['Zabudowania'] = outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie']['qgis:rastersampling_1:Zabudowania']
        results['ZabudowaniaPozaAkceptowalnymZasigiemDostpnoci'] = outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie']['native:extractbyexpression_2:Zabudowania poza akceptowalnym zasięgiem dostępności']
        results['ZabudowaniaWAkceptowalnymZasiguDostpnoci'] = outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie']['native:extractbyexpression_1:Zabudowania w akceptowalnym zasięgu dostępności']

        feedback.setCurrentStep(6)
        if feedback.isCanceled():
            return {}

        # 7. Obliczanie wyniku analizy
        alg_params = {
            'Analizowaneobiektyreprezentacjapunktowa': outputs['PobieranieObiektwDoAnalizy']['native:addautoincrementalfield_1:Analizowane obiekty – reprezentacja punktowa'],
            'Obszaranalizy': parameters['Obszaranalizy'],
            'Zabudowaniapozaakceptowalnymzasigiemdostpnoci': outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie']['native:extractbyexpression_2:Zabudowania poza akceptowalnym zasięgiem dostępności'],
            'Zabudowaniawakceptowalnymzasigudostpnoci': outputs['PrbkowanieRastrowejWarstwyDostpnociZaPomocPunktwReprezentujcychZabudowaniaIIchWstpnyPodziaNaMieszczceSiWAkceptowalnyZasiguDostpnociLubNie']['native:extractbyexpression_1:Zabudowania w akceptowalnym zasięgu dostępności'],
            'qgis:fieldcalculator_6:Podsumowanie analizy': parameters['PodsumowanieAnalizy']
        }
        outputs['ObliczanieWynikuAnalizy'] = processing.run('model:7. Obliczanie wyniku analizy', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['PodsumowanieAnalizy'] = outputs['ObliczanieWynikuAnalizy']['qgis:fieldcalculator_6:Podsumowanie analizy']
        return results

    def name(self):
        return 'Wyznaczanie dostępności – pełny algorytm'

    def displayName(self):
        return 'Wyznaczanie dostępności – pełny algorytm'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p></p>
<h2>Parametry wejściowe</h2>
<h3>Układ współrzędnych</h3>
<p>Zdefiniuj układ współrzędnych właściwy dla obszaru analizy.

Używaj tylko układów współrzędnych prostokątnych płaskich.</p>
<h3>Obszary do analizy</h3>
<p>Obszary analizy stanowić mogą poligony (wszystkie lub wybrane) ze wskazanej warstwy wektorowej projektu. Analiza zostanie przeprowadzona dla każdego poligonu oddzielnie.</p>
<h3>Klucz OSM</h3>
<p>Typ klucza dla tagów OpenStreetMap.

Tagi OSM przyjmują postać "klucz"='wartość'. Tu należy zdefiniować pierwszą z nich.

Domyślnie "amenity" co jest odpowiednie dla wyszukiwania obiektów użyteczności.</p>
<h3>Obiekty do analizy</h3>
<p>Wskaż typ analizowanych obiektów (wartości klucza "amenity" z OpenStreetMap lub innego klucza, jeśli w polu powyżej zmieniłeś ustawienia domyślne).

Lista wartości klucza "amenity" znajduje się na stronie: https://wiki.openstreetmap.org/wiki/Pl:Key:amenity</p>
<h3>Odległość analizy od znalezionych obiektów</h3>
<p>Podaj maksymalny zasięg od wyszukiwanych obiektów dla jakiego będziesz chciał przeprowadzać analizę dostępności. Ze względów na ograniczoną wydajność znacznej części komputerów domowych wprowadzono ograniczenie wynoszące od 500 do 10000 jednostek we wskazanym układzie współrzędnych.</p>
<h3>Wyrażenie filta</h3>
<p>Podaj zapytanie, które posłuży jako filtr.

Przykładowe zapytania:
—  odrzuci z daleszej analizy autostrady, drogi ekpresowe, ich łącznice, drogi planowne i w budowie (domyślne, przydatne dla analizy dostępności pieszej):
"highway" not in ('motorway', 'motorway_link', 'trunk', 'trunk_link', 'proposed', 'construction')
— odrzuci z dalszej analizy drogi planowane i w budowie oraz ciągi nieprzeznaczone dla ruchu samochodów (ścieżki, chodniki, drogi rowerowe) ( przydatne do analizy dostępności samochodowej):
"highway" not in ('proposed', 'construction', 'path', 'footway', 'cycleway')

Usuwając drogi w budowie ("highway"='construction') należy zachować szczególną ostrożność, gdzyż tym samym tagiem ozanczane są drogi w przebudowie lub remoncie, które jednak dalej częściowo pozostają przejezdne, a w związku z tym zapewniają obsługę dla terenów położonych bezpośrednio przy nich lub położonych dalej. Najczęściej konieczna jest manualna weryfikacja danych.</p>
<h3>Interwał izopoligonów</h3>
<p>Odległość co jaką algorytm wyznaczania dostępności wykreśli granicę izopoligonów dostępności.</p>
<h3>Akceptowalny zasięg dostępności</h3>
<p>Odległość od obiektu dla jakiej zabudowania zostaną uznane za mieszczące się w strefie dostępności tego obiektu.</p>
<h3>Verbose logging</h3>
<p></p>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p></p>
<h3>Siatka drogowa – obiekty niespełniające warunku filtrowania</h3>
<p></p>
<h3>Siatka drogowa – obiekty spełniające warunek fitrowania</h3>
<p></p>
<h3>Poligony zasięgu dostępności</h3>
<p></p>
<h3>Zinterpolowana dostępność</h3>
<p></p>
<h3>Zabudowania</h3>
<p></p>
<h3>Zabudowania poza akceptowalnym zasięgiem dostępności</h3>
<p></p>
<h3>Zabudowania w akceptowalnym zasięgu dostępności</h3>
<p></p>
<h3>Podsumowanie analizy</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<h3>Siatka drogowa – reprezentacja liniowa</h3>
<p></p>
<h3>Siatka drogowa – obiekty niespełniające warunku filtrowania</h3>
<p></p>
<h3>Siatka drogowa – obiekty spełniające warunek fitrowania</h3>
<p></p>
<h3>Poligony zasięgu dostępności</h3>
<p></p>
<h3>Zinterpolowana dostępność</h3>
<p></p>
<h3>Zabudowania</h3>
<p></p>
<h3>Zabudowania poza akceptowalnym zasięgiem dostępności</h3>
<p></p>
<h3>Zabudowania w akceptowalnym zasięgu dostępności</h3>
<p></p>
<h3>Podsumowanie analizy</h3>
<p></p>
<br></body></html>"""

    def createInstance(self):
        return WyznaczanieDostpnociPenyAlgorytm()
