"""
Model exported as python.
Name : 7. Obliczanie wyniku analizy
Group : Automatyzacja wyzanczania dostępności
With QGIS : 31401
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterFeatureSource
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSink
import processing


class ObliczanieWynikuAnalizy(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterFeatureSource('Obszaranalizy', 'Obszar analizy', types=[QgsProcessing.TypeVectorPolygon], defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSource('Analizowaneobiektyreprezentacjapunktowa', 'Analizowane obiekty – reprezentacja punktowa', types=[QgsProcessing.TypeVectorPoint], defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))
        self.addParameter(QgsProcessingParameterFeatureSource('Zabudowaniapozaakceptowalnymzasigiemdostpnoci', 'Zabudowania poza akceptowalnym zasięgiem dostępności', types=[QgsProcessing.TypeVectorPoint], defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSource('Zabudowaniawakceptowalnymzasigudostpnoci', 'Zabudowania w akceptowalnym zasięgu dostępności', types=[QgsProcessing.TypeVectorPoint], defaultValue=None))
        self.addParameter(QgsProcessingParameterFeatureSink('PodsumowanieAnalizy', 'Podsumowanie analizy', type=QgsProcessing.TypeVectorAnyGeometry, createByDefault=True, defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(10, model_feedback)
        results = {}
        outputs = {}

        # Policz powierzchnię obszarów analizy
        alg_params = {
            'FIELD_LENGTH': 10,
            'FIELD_NAME': 'Pow',
            'FIELD_PRECISION': 3,
            'FIELD_TYPE': 0,
            'FORMULA': ' $area ',
            'INPUT': parameters['Obszaranalizy'],
            'NEW_FIELD': True,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczPowierzchniObszarwAnalizy'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Policz obiekty w obszarach analizy
        alg_params = {
            'CLASSFIELD': '',
            'FIELD': 'LOb',
            'POINTS': parameters['Analizowaneobiektyreprezentacjapunktowa'],
            'POLYGONS': outputs['PoliczPowierzchniObszarwAnalizy']['OUTPUT'],
            'WEIGHT': '',
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczObiektyWObszarachAnalizy'] = processing.run('native:countpointsinpolygon', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Policz zabudowania w akceptowalnym zasięgu dostępności mieszczące się w obszarze analizy
        alg_params = {
            'CLASSFIELD': '',
            'FIELD': 'LwZas',
            'POINTS': parameters['Zabudowaniawakceptowalnymzasigudostpnoci'],
            'POLYGONS': outputs['PoliczPowierzchniObszarwAnalizy']['OUTPUT'],
            'WEIGHT': '',
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczZabudowaniaWAkceptowalnymZasiguDostpnociMieszczceSiWObszarzeAnalizy'] = processing.run('native:countpointsinpolygon', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Policz zabudowania poza akceptowalnym zasięgiem dostępności mieszczące się w obszarze analizy
        alg_params = {
            'CLASSFIELD': '',
            'FIELD': 'LpzZas',
            'POINTS': parameters['Zabudowaniapozaakceptowalnymzasigiemdostpnoci'],
            'POLYGONS': outputs['PoliczPowierzchniObszarwAnalizy']['OUTPUT'],
            'WEIGHT': '',
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczZabudowaniaPozaAkceptowalnymZasigiemDostpnociMieszczceSiWObszarzeAnalizy'] = processing.run('native:countpointsinpolygon', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        # Liczba zabudoawń w i poza akceptowalnym zasięgiem
        alg_params = {
            'DISCARD_NONMATCHING': True,
            'INPUT': outputs['PoliczZabudowaniaWAkceptowalnymZasiguDostpnociMieszczceSiWObszarzeAnalizy']['OUTPUT'],
            'JOIN': outputs['PoliczZabudowaniaPozaAkceptowalnymZasigiemDostpnociMieszczceSiWObszarzeAnalizy']['OUTPUT'],
            'JOIN_FIELDS': [''],
            'METHOD': 1,
            'PREDICATE': [2],
            'PREFIX': '',
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['LiczbaZabudoawWIPozaAkceptowalnymZasigiem'] = processing.run('native:joinattributesbylocation', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(5)
        if feedback.isCanceled():
            return {}

        # Liczba zabudoawń wg akceptowalnego zasięgiu oraz liczba obiektów w obszarach analizy
        alg_params = {
            'DISCARD_NONMATCHING': True,
            'INPUT': outputs['LiczbaZabudoawWIPozaAkceptowalnymZasigiem']['OUTPUT'],
            'JOIN': outputs['PoliczObiektyWObszarachAnalizy']['OUTPUT'],
            'JOIN_FIELDS': [''],
            'METHOD': 1,
            'PREDICATE': [2],
            'PREFIX': '',
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['LiczbaZabudoawWgAkceptowalnegoZasigiuOrazLiczbaObiektwWObszarachAnalizy'] = processing.run('native:joinattributesbylocation', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(6)
        if feedback.isCanceled():
            return {}

        # Policz sumę zabudowań
        alg_params = {
            'FIELD_LENGTH': 100000,
            'FIELD_NAME': 'SumaZab',
            'FIELD_PRECISION': 0,
            'FIELD_TYPE': 1,
            'FORMULA': '\"LwZas\"+\"LpzZas\"',
            'INPUT': outputs['LiczbaZabudoawWgAkceptowalnegoZasigiuOrazLiczbaObiektwWObszarachAnalizy']['OUTPUT'],
            'NEW_FIELD': True,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczSumZabudowa'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(7)
        if feedback.isCanceled():
            return {}

        # Policz procent zabudowań w akceptowalnym zasięgu dostępności
        alg_params = {
            'FIELD_LENGTH': 100000,
            'FIELD_NAME': '%ZabwZas',
            'FIELD_PRECISION': 0,
            'FIELD_TYPE': 1,
            'FORMULA': '\"LwZas\"/\"SumaZab\"*100',
            'INPUT': outputs['PoliczSumZabudowa']['OUTPUT'],
            'NEW_FIELD': True,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczProcentZabudowaWAkceptowalnymZasiguDostpnoci'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(8)
        if feedback.isCanceled():
            return {}

        # Policz procent zabudowań poza akceptowalnym zasięgiem dostępności
        alg_params = {
            'FIELD_LENGTH': 100000,
            'FIELD_NAME': '%ZabpzZas',
            'FIELD_PRECISION': 0,
            'FIELD_TYPE': 1,
            'FORMULA': '\"LpzZas\"/\"SumaZab\"*100',
            'INPUT': outputs['PoliczProcentZabudowaWAkceptowalnymZasiguDostpnoci']['OUTPUT'],
            'NEW_FIELD': True,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['PoliczProcentZabudowaPozaAkceptowalnymZasigiemDostpnoci'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(9)
        if feedback.isCanceled():
            return {}

        # Policz gęstość analizowanych obiektów na analizowanych obszarach
        alg_params = {
            'FIELD_LENGTH': 100000,
            'FIELD_NAME': 'GestOb',
            'FIELD_PRECISION': 3,
            'FIELD_TYPE': 0,
            'FORMULA': '\"LOb\"/\"Pow\"',
            'INPUT': outputs['PoliczProcentZabudowaPozaAkceptowalnymZasigiemDostpnoci']['OUTPUT'],
            'NEW_FIELD': True,
            'OUTPUT': parameters['PodsumowanieAnalizy']
        }
        outputs['PoliczGstoAnalizowanychObiektwNaAnalizowanychObszarach'] = processing.run('qgis:fieldcalculator', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        results['PodsumowanieAnalizy'] = outputs['PoliczGstoAnalizowanychObiektwNaAnalizowanychObszarach']['OUTPUT']
        return results

    def name(self):
        return '7. Obliczanie wyniku analizy'

    def displayName(self):
        return '7. Obliczanie wyniku analizy'

    def group(self):
        return 'Automatyzacja wyzanczania dostępności'

    def groupId(self):
        return 'Automatyzacja wyzanczania dostępności'

    def shortHelpString(self):
        return """<html><body><h2>Opis algorytmu</h2>
<p></p>
<h2>Parametry wejściowe</h2>
<h3>Obszar analizy</h3>
<p></p>
<h3>Analizowane obiekty – reprezentacja punktowa</h3>
<p></p>
<h3>Verbose logging</h3>
<p></p>
<h3>Zabudowania poza akceptowalnym zasięgiem dostępności</h3>
<p></p>
<h3>Zabudowania w akceptowalnym zasięgu dostępności</h3>
<p></p>
<h3>Podsumowanie analizy</h3>
<p></p>
<h2>Wyniki</h2>
<h3>Podsumowanie analizy</h3>
<p></p>
<br></body></html>"""

    def createInstance(self):
        return ObliczanieWynikuAnalizy()
